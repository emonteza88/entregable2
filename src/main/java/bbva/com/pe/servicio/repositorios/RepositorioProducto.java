package bbva.com.pe.servicio.repositorios;

import bbva.com.pe.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioProducto extends MongoRepository<Producto, String> {
}
