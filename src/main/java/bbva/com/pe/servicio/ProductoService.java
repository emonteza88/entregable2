package bbva.com.pe.servicio;

import bbva.com.pe.modelo.Producto;
import org.springframework.data.domain.Page;


public interface ProductoService {

    public Page<Producto> obtenerProductos(int pagina, int tamanio);

    public void agregarProducto(Producto producto);

    public void updateProducto(String codigo, Producto producto);

    public void deleteProducto(String codigo);
}
